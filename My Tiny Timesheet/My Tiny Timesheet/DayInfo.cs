﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Xml;

namespace My_Tiny_Timesheet
{
    class DayInfo
    {
        private int dayOfMonth;
        private DayOfWeek dayOfWeek;
        private List<DayTime> unfilled = new List<DayTime>();
        private XmlDocument infoFile;
        private TimeSpan hstart;
        private TimeSpan hlunch;
        private TimeSpan haft;
        private TimeSpan hend;

        /// <summary>
        /// Creates an XMLFile for a new month. This case happens if the file is not existing
        /// </summary>
        /// <param name="dayOfWeek"></param>
        /// <param name="dayOfMonth"></param>
        /// <param name="saveFile"></param>
        public DayInfo(DayOfWeek dayOfWeek, int dayOfMonth, XmlDocument saveFile)
        {
            this.dayOfWeek = dayOfWeek;
            this.dayOfMonth = dayOfMonth;
            unfilled.Add(DayTime.Morning);
            unfilled.Add(DayTime.Lunch);
            unfilled.Add(DayTime.Afternoon);
            unfilled.Add(DayTime.Home);
            infoFile = saveFile;

            foreach (XmlElement dayNode in saveFile.GetElementsByTagName("Day"))
            {
                string id_Attribute = dayNode.GetAttribute("id");
                if (id_Attribute == dayOfMonth.ToString())
                    return;
            }
            XmlElement day = infoFile.CreateElement("Day");
            day.SetAttribute("day", dayOfWeek.ToString());
            day.SetAttribute("id", dayOfMonth.ToString());
            saveFile.LastChild.AppendChild(day);
        }

        public DayInfo(XmlElement dayNode)
        {
            unfilled.Add(DayTime.Morning);
            unfilled.Add(DayTime.Lunch);
            unfilled.Add(DayTime.Afternoon);
            unfilled.Add(DayTime.Home);
            dayOfWeek = Utils.getDayOfWeek(dayNode.GetAttribute("day"));
            dayOfMonth = int.Parse(dayNode.GetAttribute("id"));
            for (int i = 0; i < dayNode.ChildNodes.Count; i++)
            {
                XmlNode node = dayNode.ChildNodes[i];
                switch (node.Name)
                {
                    case "hstart":
                        hstart = TimeSpan.Parse(node.InnerText);
                        unfilled.Remove(DayTime.Morning);
                        break;
                    case "hlunch":
                        hlunch = TimeSpan.Parse(node.InnerText);
                        unfilled.Remove(DayTime.Lunch);
                        break;
                    case "haft":
                        haft = TimeSpan.Parse(node.InnerText);
                        unfilled.Remove(DayTime.Afternoon);
                        break;
                    case "hend":
                        hend = TimeSpan.Parse(node.InnerText);
                        unfilled.Remove(DayTime.Home);
                        break;
                }
            }
        }

        public DayInfo(int dayOfMonth, XmlDocument existingSaveFile)
        {
            this.dayOfMonth = dayOfMonth;
            infoFile = existingSaveFile;
            unfilled.Add(DayTime.Morning);
            unfilled.Add(DayTime.Lunch);
            unfilled.Add(DayTime.Afternoon);
            unfilled.Add(DayTime.Home);

            XmlElement todayNode = null;
            foreach (XmlElement dayNode in infoFile.GetElementsByTagName("Day"))
            {
                string id_Attribute = dayNode.GetAttribute("id");
                if (id_Attribute == dayOfMonth.ToString())
                {
                    todayNode = dayNode;
                    break;
                }
            }
            if (todayNode == null)
                throw new ArgumentNullException();
            dayOfWeek = Utils.getDayOfWeek(todayNode.GetAttribute("day"));
            for (int i = 0; i < todayNode.ChildNodes.Count; i++)
            {
                XmlNode node = todayNode.ChildNodes[i];
                try
                {
                    switch (node.Name)
                    {
                        case "hstart":
                            hstart = TimeSpan.Parse(node.InnerText);
                            unfilled.Remove(DayTime.Morning);
                            break;
                        case "hlunch":
                            hlunch = TimeSpan.Parse(node.InnerText);
                            unfilled.Remove(DayTime.Lunch);
                            break;
                        case "haft":
                            haft = TimeSpan.Parse(node.InnerText);
                            unfilled.Remove(DayTime.Afternoon);
                            break;
                        case "hend":
                            hend = TimeSpan.Parse(node.InnerText);
                            unfilled.Remove(DayTime.Home);
                            break;
                    }
                }
                catch (Exception e)
                {
                    if (e is FormatException || e is ArgumentNullException)
                    {
                        todayNode.RemoveChild(node);
                        i--;
                    }
                    existingSaveFile.Save(existingSaveFile.BaseURI.Remove(0, 8));
                }
            }
        }

        public void addHour(TimeSpan hour, DayTime state)
        {
            XmlElement newElt = null;
            XmlNode dayNode = infoFile.DocumentElement.LastChild;

            string messageBoxText = '\"' + state.ToString() + "\" information already reported for this shift. Overwrite?";
            string sCaption = "Information already filled";

            if (!unfilled.Contains(state))
            {
                MessageBoxResult res = Utils.displayPopup(sCaption, messageBoxText, MessageBoxButton.YesNoCancel);
                if (res == MessageBoxResult.No)
                    return;
                else if (res == MessageBoxResult.Cancel)
                    //Stay in this panel. Maybe return a bool if everything is ok or "no"
                    return;
                    
            }
            // Pour chaque cas : cree un element <h???>, set l'inner text par l'heure, ajoute l'element a sa position dans le xml
            switch (state)
            {
                case DayTime.Morning:
                    newElt = dayNode.SelectSingleNode("hstart") as XmlElement;
                    if (hour == TimeSpan.Zero)
                    {
                        dayNode.RemoveChild(newElt);
                        return;
                    }
                    hstart = hour;
                    if (newElt == null)
                        newElt = infoFile.CreateElement("hstart");
                    newElt.InnerText = hour.ToString("hh\\:mm\\:ss");
                    dayNode.InsertBefore(newElt, dayNode.FirstChild);
                    break;
                case DayTime.Lunch:
                    newElt = dayNode.SelectSingleNode("hlunch") as XmlElement;
                    if (hour == TimeSpan.Zero)
                    {
                        dayNode.RemoveChild(newElt);
                        return;
                    }
                    hlunch = hour;
                    if (newElt == null)
                        newElt = infoFile.CreateElement("hlunch");
                    newElt.InnerText = hour.ToString("hh\\:mm\\:ss");
                    if (dayNode.FirstChild != null && dayNode.FirstChild.Name != "hstart")
                        dayNode.InsertBefore(newElt, dayNode.FirstChild);
                    else
                        dayNode.InsertAfter(newElt, dayNode.FirstChild);
                    break;
                case DayTime.Afternoon:
                    newElt = dayNode.SelectSingleNode("haft") as XmlElement;
                    if (hour == TimeSpan.Zero)
                    {
                        dayNode.RemoveChild(newElt);
                        return;
                    }
                    haft = hour;
                    if (newElt == null)
                        newElt = infoFile.CreateElement("haft");
                    newElt.InnerText = hour.ToString("hh\\:mm\\:ss");
                    if (dayNode.FirstChild != null && dayNode.LastChild.Name != "hend")
                        dayNode.InsertAfter(newElt, dayNode.LastChild);
                    else
                        dayNode.InsertBefore(newElt, dayNode.LastChild);
                    break;
                default:
                    newElt = dayNode.SelectSingleNode("hend") as XmlElement;
                    if (hour == TimeSpan.Zero)
                    {
                        dayNode.RemoveChild(newElt);
                        return;
                    }
                    hend = hour;
                    if (newElt == null)
                        newElt = infoFile.CreateElement("hend");
                    newElt.InnerText = hour.ToString("hh\\:mm\\:ss");
                    dayNode.AppendChild(newElt);
                    break;
            }
        }

        public TimeSpan getDayWorkingHours(DayTime dayTime = 0)
        {
            bool isToday = dayOfMonth == DateTime.Now.Day;// && infoFile.FirstChild.Name == Utils.getMonthByLanguage(DateTime.Now.Month);
            TimeSpan morningHours = (hlunch != TimeSpan.Zero && hstart != TimeSpan.Zero) ? hlunch.Subtract(hstart) : ((isToday && dayTime == DayTime.Morning) ? DateTime.Now.TimeOfDay.Subtract(hstart) : TimeSpan.Zero);
            TimeSpan afternoonHours = (hend != TimeSpan.Zero && haft != TimeSpan.Zero) ? hend.Subtract(haft) : ((isToday && dayTime == DayTime.Afternoon) ? DateTime.Now.TimeOfDay.Subtract(haft) : TimeSpan.Zero);

            /*
            TimeSpan morningHours;
            TimeSpan afternoonHours;
            if (hlunch != TimeSpan.Zero && hstart != TimeSpan.Zero)
                morningHours = hlunch.Subtract(hstart);
            else
            {
                if (isToday && dayTime == DayTime.Morning)
                    morningHours = DateTime.Now.TimeOfDay.Subtract(hstart);
                else
                    morningHours = TimeSpan.Zero;
            }

            if (hend != TimeSpan.Zero && haft != TimeSpan.Zero)
                afternoonHours = hend.Subtract(haft);
            else
            {
                if (isToday && dayTime == DayTime.Afternoon)
                    afternoonHours = DateTime.Now.TimeOfDay.Subtract(haft);
                else
                    afternoonHours = TimeSpan.Zero;
            }
            */

            if (morningHours > TimeSpan.Zero && afternoonHours > TimeSpan.Zero)
                return morningHours.Add(afternoonHours);
            else if (morningHours > TimeSpan.Zero || afternoonHours > TimeSpan.Zero)
                return (morningHours > afternoonHours) ? morningHours : afternoonHours;
            else
                return TimeSpan.Zero;
        }

        public bool dayStarted()
        {
            TimeSpan zero = TimeSpan.Zero;
            return (hstart != zero || hlunch != zero || haft != zero || hend != zero);
        }

        #region Getters/Setters
        public List<DayTime> Unfilled
        {
            get { return unfilled; }
        }

        public TimeSpan Hstart
        {
            get { return hstart; }
        }

        public TimeSpan Hend
        {
            get { return hend; }
        }

        public TimeSpan Haft
        {
            get { return haft; }
        }

        public TimeSpan Hlunch
        {
            get { return hlunch; }
        }

        public int DayOfMonth
        {
            get { return dayOfMonth; }
        }

        public DayOfWeek DayOfWeek
        {
            get { return dayOfWeek; }
        }

        //public void setHour
        #endregion
    }
}
