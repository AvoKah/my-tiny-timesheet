﻿using System;
using System.Globalization;
using System.Windows;

namespace My_Tiny_Timesheet
{
    public enum DayTime
    {
        Morning,
        Lunch,
        Afternoon,
        Home
    }

    public static class Utils
    {
        public static DayTime getDayTime(string dayTime)
        {
            switch (dayTime.ToLower())
            {
                case "morning":
                case "hstart":
                    return DayTime.Morning;
                case "lunch":
                case "hlunch":
                    return DayTime.Lunch;
                case "afternoon":
                case "haft":
                    return DayTime.Afternoon;
                default:
                    return DayTime.Home;
            }
        }

        public static DayOfWeek getDayOfWeek(string day)
        {
            for (int i = 0; i < 7; i++)
                if (((DayOfWeek)i).ToString().ToLower() == day.ToLower())
                    return (DayOfWeek)(i);
            return 0;
        }

        public static MessageBoxResult displayPopup(string title, string message, MessageBoxButton btnType = MessageBoxButton.OK, MessageBoxImage icon = MessageBoxImage.Warning)
        {
            return MessageBox.Show(message, title, btnType, icon);
        }

        public static string getMonthByLanguage(int month, string language = "fr")
        {
            CultureInfo culture = (language.ToLower() == "fr") ? new CultureInfo("fr-FR") : new CultureInfo("en-EN");
            return culture.DateTimeFormat.GetMonthName(month);
        }

        public static string getDayByLanguage(DayOfWeek day, string language = "fr")
        {
            CultureInfo culture = (language.ToLower() == "fr") ? new CultureInfo("fr-FR") : new CultureInfo("en-EN");
            return culture.DateTimeFormat.GetDayName(day);
        }
        public static string firstLetterUpperCase(string str)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str.ToLower());
        }
    }
}
