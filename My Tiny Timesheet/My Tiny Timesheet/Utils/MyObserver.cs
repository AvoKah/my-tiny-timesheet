﻿using My_Tiny_Timesheet;

namespace Working_Hours_Counter.Utils
{
    public interface MyObserver
    {
        //void update();

        void update(DayTimeField dayTimeField);
    }
}
