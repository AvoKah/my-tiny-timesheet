﻿using System;
using System.Windows.Controls;

namespace My_Tiny_Timesheet
{
    public class DayTimeField
    {
        private Label label;
        private TimeSpan timeSpan;
        private TextBlock textBlock;
        private Button button;
        private TextBox textBox;
        private DayTime dayTime;

        public DayTimeField(Label label, TimeSpan timeSpan, TextBlock textBlock, Button button, DayTime dayTime, TextBox textBox = null)
        {
            this.label = label;
            this.timeSpan = timeSpan;
            this.textBlock = textBlock;
            this.button = button;
            this.dayTime = dayTime;
            this.textBox = textBox;
        }

        public Label Lbl
        {
            get { return label; }
        }

        public TimeSpan Timespan
        {
            get { return timeSpan; }
            set { timeSpan = value; }
        }

        public TextBlock Textblock
        {
            get { return textBlock; }
        }

        public Button Buttn
        {
            get { return button; }
        }

        public DayTime Daytime
        {
            get { return dayTime; }
        }

        public TextBox Textbox
        {
            get { return textBox; }
        }
    }
}
