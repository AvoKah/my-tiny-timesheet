﻿using My_Tiny_Timesheet;

namespace Working_Hours_Counter.Utils
{
    public interface MyObservable
    {
        void addObserver(MyObserver o);
        void notify(DayTimeField dayTimeField);
    }
}
