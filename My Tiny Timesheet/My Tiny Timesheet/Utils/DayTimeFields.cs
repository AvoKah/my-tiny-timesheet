﻿using System.Collections.Generic;

namespace My_Tiny_Timesheet
{
    static class DayTimeFields
    {
        private static DayTimeField hstart;
        private static DayTimeField hlunch;
        private static DayTimeField haft;
        private static DayTimeField hend;

        public static void setDayInfo(DayInfo dayInfo)
        {
            hstart.Timespan = dayInfo.Hstart;
            hlunch.Timespan = dayInfo.Hlunch;
            haft.Timespan = dayInfo.Haft;
            hend.Timespan = dayInfo.Hend;
        }
            
        internal static DayTimeField Hstart
        {
            get { return hstart; }
            set { hstart = value; }
        }

        internal static DayTimeField Hlunch
        {
            get { return hlunch; }
            set { hlunch = value; }
        }

        internal static DayTimeField Haft
        {
            get { return haft; }
            set { haft = value; }
        }

        internal static DayTimeField Hend
        {
            get { return hend; }
            set { hend = value; }
        }
        
        // A bit nasty here. See if I can make it cleaner
        public static List<DayTimeField> List
        {
            get
            {
                return new List<DayTimeField>() { hstart, hlunch, haft, hend };
            }
        }
    }
}
