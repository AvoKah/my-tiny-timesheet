﻿using System.Windows.Controls;
using System.Windows;
using System;
using System.Windows.Media;
using Working_Hours_Counter.Utils;
using System.Collections.Generic;

namespace My_Tiny_Timesheet.Resources.UserControls
{
	/// <summary>
	/// Interaction logic for DayReview.xaml
	/// </summary>
	public partial class DayReview : UserControl, MyObservable
	{
        //private MainWindow mainWindow;
        private List<MyObserver> observers;

		public DayReview()
		{
			InitializeComponent();
            observers = new List<MyObserver>();
		}

        private void label_PreviewMouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Label clicked = sender as Label;
            clicked.Visibility = Visibility.Collapsed;

            DayTimeField daytimeChanged = DayTimeFields.List.Find(daytime => daytime.Lbl == clicked);
            daytimeChanged.Textbox.Visibility = Visibility.Visible;
            daytimeChanged.Textbox.Text = clicked.Content.ToString();
            daytimeChanged.Textbox.Focus();
        }

        private void textBox_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox previouslyFocusedTextBox = sender as TextBox;
            previouslyFocusedTextBox.Visibility = Visibility.Collapsed;

            DayTimeField daytimeLostFocus = DayTimeFields.List.Find(daytime => daytime.Textbox == previouslyFocusedTextBox);
            daytimeLostFocus.Lbl.Visibility = Visibility.Visible;
        }

        private void textBox_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            TextBox focused = sender as TextBox;
            DayTimeField daytimeFocused = DayTimeFields.List.Find(daytime => daytime.Textbox == focused);
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                focused.Visibility = Visibility.Collapsed;
                daytimeFocused.Lbl.Visibility = Visibility.Visible;
                TimeSpan newHour = TimeSpan.Parse(focused.Text);
                daytimeFocused.Lbl.Content = newHour.ToString("hh\\:mm");
                try
                {
                    if (newHour == TimeSpan.Zero)
                    {
                        daytimeFocused.Lbl.ClearValue(ForegroundProperty);
                        daytimeFocused.Textblock.ClearValue(ForegroundProperty);
                    }
                    else
                    {
                        daytimeFocused.Lbl.Foreground = Brushes.White;
                        daytimeFocused.Textblock.Foreground = Brushes.White;
                    }
                    notify(daytimeFocused);
                } catch (Exception ex)
                {
                    Utils.displayPopup(ex.Source, "Wrong format: " + ex.Message);
                    return;
                }
            }
            if (e.Key == System.Windows.Input.Key.Escape)
            {
                focused.Visibility = Visibility.Collapsed;
                daytimeFocused.Lbl.Visibility = Visibility.Visible;
            }
        }

        public void addObserver(MyObserver o)
        {
            observers.Add(o);
        }

        public void notify(DayTimeField dayTimeField)
        {
            foreach (MyObserver o in observers)
                o.update(dayTimeField);
        }
    }
}
