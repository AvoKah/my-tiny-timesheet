﻿using System;
using System.Collections.ObjectModel;
using System.Windows;

namespace My_Tiny_Timesheet
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		public void ApplySkin(Uri skinDictionaryUri)
		{
			// Load the ResourceDictionary into memory.
			ResourceDictionary skinDict = new ResourceDictionary();
			skinDict.Source = new Uri(skinDictionaryUri.ToString(), UriKind.RelativeOrAbsolute);
			Collection<ResourceDictionary> mergedDicts = Resources.MergedDictionaries;

			// Remove the existing skin dictionary, if one exists.
			// NOTE: In a real application, this logic might need
			// to be more complex, because there might be dictionaries
			// which should not be removed.
			if (mergedDicts.Count > 0)
				mergedDicts.Clear();

			// Apply the selected skin so that all elements in the
			// application will honor the new look and feel.
			mergedDicts.Add(skinDict);
		}
	}
}
