﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;
using System.Xml;
using My_Tiny_Timesheet.Resources.UserControls;
using Working_Hours_Counter.Utils;

namespace My_Tiny_Timesheet
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, MyObserver
    {
        private DayInfo     workDay;
        private DayTime     dayTime;
        private DayReview   dayReview;
        private XmlDocument xmlFile;
        private string      month;
        private string      xmlFilePath;
        private bool        debug = true;

        public MainWindow()
        {
            //debug = false;
            InitializeComponent();

            month = Utils.getMonthByLanguage(DateTime.Now.Month);
            dayReview = new DayReview();
            dayReview.addObserver(this);

            getDayTime();
            init();

            ApplySkinFromUri();
        }

        private void getDayTime()
        {
            DateTime now = DateTime.Now;
            DateTime lunch = new DateTime(now.Year, now.Month, now.Day, 11, 30, 00);
            DateTime aft = new DateTime(now.Year, now.Month, now.Day, 13, 30, 00);
            DateTime end = new DateTime(now.Year, now.Month, now.Day, 16, 30, 00);
            if (now < lunch)
                dayTime = DayTime.Morning;
            else if (now > lunch && now < aft)
                dayTime = DayTime.Lunch;
            else if (now > aft && now < end)
                dayTime = DayTime.Afternoon;
            else
                dayTime = DayTime.Home;
        }

        private void init_hours()
        {
            DayTimeFields.Hstart = new DayTimeField(dayReview.hstartLabel, workDay.Hstart, dayReview.hstartTextBlock, hstartButton, (DayTime)0, dayReview.hstartTextBox);
            DayTimeFields.Hlunch = new DayTimeField(dayReview.hlunchLabel, workDay.Hlunch, dayReview.hlunchTextBlock, hlunchButton, (DayTime)1, dayReview.hlunchTextBox);
            DayTimeFields.Haft = new DayTimeField(dayReview.haftLabel, workDay.Haft, dayReview.haftTextBlock, haftButton, (DayTime)2, dayReview.haftTextBox);
            DayTimeFields.Hend = new DayTimeField(dayReview.hendLabel, workDay.Hend, dayReview.hendTextBlock, hendButton, (DayTime)3, dayReview.hendTextBox);
        }

        private void init()
        {
            // Setting the path of the output file
            string folder = "C:\\Users\\" + Environment.UserName + "\\";
            if (!Directory.Exists(folder + ".workingHours"))
            {
                DirectoryInfo di = Directory.CreateDirectory(folder + ".workingHours");
                di.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
            }
            xmlFilePath = getFilePath(DateTime.Now.Month, DateTime.Now.Year);

            // Setting the real-time displayed clock
            lblHour.Content = DateTime.Now.ToString("HH:mm:sss");
            DispatcherTimer timer = new DispatcherTimer(new TimeSpan(0, 0, 1), DispatcherPriority.Normal, delegate
            {
                lblHour.Content = DateTime.Now.ToString("HH:mm:ss");
            }, Dispatcher);

            // Loading/Creating the output file
            xmlFile = new XmlDocument();
            if (File.Exists(xmlFilePath))
            {
                xmlFile.Load(xmlFilePath);
                try
                {
                    // Voir si le node du jour est déja créé
                    workDay = new DayInfo(DateTime.Now.Day, xmlFile);
                    if (workDay.dayStarted())
                    {
                        DayTime xmlDaytime = Utils.getDayTime(getTagnameContent("daytime"));
                        DayTime nextDayTime = (DayTime)Math.Max((int)xmlDaytime, (int)dayTime);
                        dayTime = (workDay.Unfilled.Contains(nextDayTime)) ? nextDayTime : xmlDaytime;
                    }
                    // Check quels sont les daytimes deja filled, et griser les boutons correspondants
                    init_hours();
                    foreach (DayTimeField hour in DayTimeFields.List)
                    {
                        if (hour.Timespan != TimeSpan.Zero)
                            hour.Buttn.IsEnabled = false;
                    }
                }
                catch (ArgumentNullException)
                {
                    // Cree le node pour le jour
                    workDay = new DayInfo(DateTime.Now.DayOfWeek, DateTime.Now.Day, xmlFile);
                    init_hours();
                }
            }
            else
            {
                XmlElement rootnode = xmlFile.CreateElement(char.ToUpper(month[0]) + month.Substring(1));
                xmlFile.AppendChild(rootnode);
                XmlElement cur = xmlFile.CreateElement("daytime");
                cur.InnerText = "Home";
                xmlFile.DocumentElement.AppendChild(cur);
                workDay = new DayInfo(DateTime.Now.DayOfWeek, DateTime.Now.Day, xmlFile);
                init_hours();
            }
            if (workDay.Unfilled.Count != 0)
            {
                setLabels();
                logQuestionlbl.SetValue(Grid.RowSpanProperty, 1);
                yesButton.Visibility = Visibility.Visible;
                noButton.Visibility = Visibility.Visible;
            }
            else
                displayDayReview(DateTime.Now);
            logQuestionlbl.Content = "Il est " + DateTime.Now.ToString("HH\\:mm") + ". " + logQuestionlbl.Content;
        }

        private void setLabels()
        {
            switch (dayTime)
            {
                case DayTime.Morning:
                    logQuestionlbl.Content = "Venez-vous d'arriver?";
                    yesButton.Content = "Oui";
                    noButton.Content = "Non";
                    break;
                case DayTime.Lunch:
                    logQuestionlbl.Content = "Bonap'";
                    yesButton.Content = "Merci";
                    noButton.Content = "Je ne vais pas manger";
                    break;
                case DayTime.Afternoon:
                    logQuestionlbl.Content = "Revenez-vous de pause dej?";
                    yesButton.Content = "Oui";
                    noButton.Content = "Non";
                    break;
                default:
                    logQuestionlbl.Content = "Fini pour aujourd'hui?";
                    yesButton.Content = "Ouais enfin!";
                    noButton.Content = "Non..";
                    break;
            }
        }

        // Skins
        private void ApplySkinFromUri(string skinDictPath = "./Resources/skins/skin.xaml")
        {
            Uri skinDictUri = new Uri(skinDictPath, UriKind.Relative);
            // Tell the Application to load the skin resources.
            App app = Application.Current as App;
            app.ApplySkin(skinDictUri);
        }

        // Responsive
        private void MainGrid_SizeChanged(object sender, EventArgs e)
        {
            double yScale = ActualHeight / 1080f; //ScreenHeight;
            double xScale = ActualWidth / 1920f; //ScreenWidth;
            double value = Math.Min(xScale, yScale);
            ApplicationScaleTransform.ScaleX = xScale;
            ApplicationScaleTransform.ScaleY = yScale;
        }

        private string getTagnameContent(string tagname)
        {
            return xmlFile.DocumentElement.GetElementsByTagName(tagname)[0].InnerText;
        }

        private void setTagnameContent(string tagname, string text)
        {
            xmlFile.DocumentElement.GetElementsByTagName(tagname)[0].InnerText = text;
        }

        private void displayDayReview(DateTime day)
        {
            DayInfo dayInfo;
            if (day.Day == DateTime.Now.Day && day.Month == DateTime.Now.Month &&
                day.Year == DateTime.Now.Year)
                dayInfo = workDay;
            else
                dayInfo = getDayInfo(day);
            if (dayInfo == null)
                return;
            mainDockPanel.Children.RemoveAt(mainDockPanel.Children.Count - 1);

            titleLabel.Content = Utils.getDayByLanguage(day.DayOfWeek) + " " +
                day.Day + " " + Utils.getMonthByLanguage(day.Month) +
                " " + day.Year;
            DayTimeFields.setDayInfo(dayInfo);
            foreach (DayTimeField hour in DayTimeFields.List)
            {
                if (hour.Timespan != TimeSpan.Zero)
                {
                    hour.Lbl.Foreground = Brushes.White;
                    hour.Textblock.Foreground = Brushes.White;
                }
                hour.Lbl.Content = hour.Timespan.ToString("hh\\:mm");
            }

            if (dayInfo == workDay)
            {
                DispatcherTimer timer = new DispatcherTimer(new TimeSpan(0, 0, 1), DispatcherPriority.Normal, delegate
                {
                    if (calendar.SelectedDate == null || calendar.SelectedDate.Value.DayOfYear == DateTime.Now.DayOfYear)
                        dayReview.workedHoursLabel.Content = dayInfo.getDayWorkingHours(dayTime).ToString("hh\\:mm\\:ss");
                }, Dispatcher);
            }
            else
                dayReview.workedHoursLabel.Content = dayInfo.getDayWorkingHours().ToString("hh\\:mm");


            mainDockPanel.Children.Add(dayReview);
        }

        private TimeSpan getTotalWeekHours()
        {
            List<DayInfo> week = new List<DayInfo>();
            foreach (XmlElement dayNode in xmlFile.GetElementsByTagName("Day"))
            {
                string day_Attribute = dayNode.GetAttribute("day");
                if (day_Attribute == DateTime.Now.DayOfWeek.ToString())
                {
                    int id_attr = 0;
                    XmlElement node = dayNode;
                    do
                    {
                        week.Add(new DayInfo(node));
                        node = (XmlElement)node.PreviousSibling;
                        day_Attribute = dayNode.GetAttribute("day");
                        int.TryParse(node.GetAttribute("id"), out id_attr);
                    } while (day_Attribute != "Sunday" && Math.Abs(id_attr - week[week.Count - 1].DayOfMonth) == 1);
                    break;
                }
            }
            TimeSpan sum = TimeSpan.Zero;
            foreach (DayInfo day in week)
                sum += day.getDayWorkingHours();
            return sum;
        }

        public void update(DayTimeField dayTimeField)
        {
            string newHourLabel = dayTimeField.Lbl.Content as string;
            TimeSpan newHour = TimeSpan.Parse(newHourLabel);
            workDay.addHour(newHour, dayTimeField.Daytime);
            dayReview.workedHoursLabel.Content = workDay.getDayWorkingHours().ToString("hh\\:mm");
            xmlFile.Save(xmlFilePath);
        }

        private void update_hours()
        {
            DayTimeFields.Hstart.Timespan = workDay.Hstart;
            DayTimeFields.Hlunch.Timespan = workDay.Hlunch;
            DayTimeFields.Haft.Timespan = workDay.Haft;
            DayTimeFields.Hend.Timespan = workDay.Hend;
        }

        private void save()
        {
            workDay.addHour(DateTime.Now.TimeOfDay, dayTime);
            update_hours();
            nextDayTime();
            setTagnameContent("daytime", dayTime.ToString());
            displayDayReview(DateTime.Now);
            xmlFile.Save(xmlFilePath);
        }

        private void nextDayTime()
        {
            // [Morning, Afternoon, Home]
            //                        ^    indexCurrentDaytime = 2
            int indexCurrentDaytime = workDay.Unfilled.IndexOf(dayTime);
            // [Morning, Afternoon, Home]
            //     ^
            if (++indexCurrentDaytime >= workDay.Unfilled.Count)
                indexCurrentDaytime = 0;
            // tmp = Home
            DayTime tmp = dayTime;
            if (workDay.Unfilled.Count > 1) // Otherwise, tmp = daytime and we remove this tmp afterwards. So if everything is filled, it stays on the last unfilled
                dayTime = workDay.Unfilled[indexCurrentDaytime];
            else
                dayTime = DayTime.Morning;
            // [Morning, Afternoon]
            //     ^
        }

        #region Buttons click events
        private void yesButton_Click(object sender, RoutedEventArgs e)
        {
            save();
        }

        private void noButton_Click(object sender, RoutedEventArgs e)
        {
            getDayTime();
            displayDayReview(DateTime.Now);
            // var lol = getTotalWeekHours();
        }

        private void daytimeButtons_Click(object sender, RoutedEventArgs e)
        {
            string btnName = (sender as Button).Name.Replace("Button", "");
            dayTime = Utils.getDayTime(btnName);
            setLabels();
        }

        private void Calendar_SelectedDatesChanged(object sender, SelectionChangedEventArgs e)
        {
            Calendar dateSelectedOnCalendar = sender as Calendar;
            DateTime dateSelected = dateSelectedOnCalendar.SelectedDate.Value;
            displayDayReview(dateSelected);
        }
        #endregion

        private DayInfo getDayInfo(DateTime day)
        {
            if (day > DateTime.Now)
            {
                Utils.displayPopup("Invalid Date", "The selected date is in the future.\nNo Timesheet for this day yet.", icon: MessageBoxImage.Error);
                return null;
            }
            if (day.DayOfWeek == DayOfWeek.Saturday || day.DayOfWeek == DayOfWeek.Sunday)
            {
                Utils.displayPopup("Week-end", "Fortunately we don't work on Weekends!");
                return null;
            }
            string filePath = getFilePath(day.Month, day.Year);
            if (!File.Exists(filePath))
            {
                Utils.displayPopup("File not found", "Could not load file.\n\"" +
                    Path.GetFileName(filePath) + "\" doesn't exist.", icon: MessageBoxImage.Error);
                return null;
            }
            XmlDocument xmlFile = new XmlDocument();
            xmlFile.Load(filePath);
            try
            {
                return new DayInfo(day.Day, xmlFile);
            }
            catch (ArgumentNullException)
            {
                Utils.displayPopup("Day Not found", "Informations not completed for this day.");
            }
            return null;
        }

        private string getFilePath(int month, int year)
        {
            string filename = "C:\\Users\\" + Environment.UserName + "\\";
            filename += (debug) ? "Desktop\\" : ".workingHours\\";
            filename += Utils.firstLetterUpperCase(Utils.getMonthByLanguage(month));
            filename += "-";
            filename += year;
            filename += "_Hours";
            if (debug)
                filename += " - DEBUG";
            filename += ".xml";
            return filename;
        }
    }
}
