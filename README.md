﻿# Project


My Tiny Timesheet -> To be renamed eventually

# Purpose of this proj

Having a quick summary of worked hours for a day/week/month. Just to make sure that you respect the stipulated hours in your contract. One main constraint of it is that we have to launch the app every time we :

* Arrive
* Leave for lunch
* Come back from lunch
* Are about to leave.  

Convenient.

But the actual purpose of this project is to train myself on WPF technos

# What do we have
We can now fill the infos of a complete day of work in an XML File whose path has been hardcoded in C:\Users\\\*username*\\.workingHours, thanks to a slow and disgusting GUI with ugly fonts and ugly colors.


# Maybe add UML diagrams later for fun just to show off

 using [Mermaid](https://mermaidjs.github.io/). 

```mermaid
sequenceDiagram
Alice ->> Bob: Hello Bob, how are you?
Bob-->>John: How about you John?
Bob--x Alice: I am good thanks!
Bob-x John: I am good thanks!
Note right of John: Bob thinks a long<br/>long time, so long<br/>that the text does<br/>not fit on a row.

Bob-->Alice: Checking with John...
Alice->John: Yes... John, how are you?
```

And this will produce a flow chart:

```mermaid
graph LR
A[Square Rect] -- Link text --> B((Circle))
A --> C(Round Rect)
B --> D{Rhombus}
C --> D
```